# QuickRes

## quick start
```
pip3 install quickres
```
Folder of images (not recursive)
```
quickres /input_folder /output_folder
```
Single image, specify the output image + location
```
quickres somwhere/input_image.tif elsewhere/output_image.tif
```
Single image, keep image name but specify output directory
```
quickres somwhere/input_image.tif elsewhere/output_image.tif
```
Overwrite!
```
quickres /input_folder /output_folder --overwrite
```
Use a different model+weights
```
quickres /input_folder /output_folder --model /new_model_file.h5
```

## Deeper installation instruction
Install via setup.py
```
git clone git@bitbucket.org:ggddatascience/quickres.git
cd quickres
sudo -H python3 setup.py install
```

Install via pip with wheel. Download the `whl` package file and pip install it
```
wget quickres-2018.7.24.3-py3-none-any.whl
pip3 install quickres-2018.7.24.3-py3-none-any.whl
```

## What is QuickRes?
QuickRes is a method of applying SuperResolution on satellite imagery.
Simply, it enhances the resolution of an image to double the resolution from
NxM to 2Nx2M.

### How?
Originally, Fabio Pacifici created a process (HD) that would improve
the visual resolution using a single image and no history.
Using a few of Fabio's images, we created a neural network that would read
5x5 segments of the low-res image and effectively "replace" the center pixel
with a set of 2x2 pixels, thus being a purely convolutional approach.
As the network shifts pixel by pixel across an image, new 2x2 pixels are
produced. The reasoning behind the 5x5 image segment is that to produce the
upscaled pixels, the surrounding pixels around the center pixel provide "context"
for the neural network to produce the upscaled pixels.
This is in contrast with a bilinear interpolation approach that produces
new "pixels" from "averaging" two pixels to produce the new pixel in between the original pixels.
The network produces pixels as seen below.
The "stair-steps" of white/black pixels along the plane are reduced down
to being not so jagged by reducing the stair steps.


<img src="planelr.png" height="300" />
<img src="planeqr.png" height="300" />

#### Network Architecture
The network architecture is as follows:

|Layer   |   |   |   |
|---|---|---|---|
| 1 |conv 5x5kernel, 256, same padding   | LeakyRelu  |   |
| 2 |conv 1x1kernel,    |   |   |
|   |   |   |   |
|   |   |   |   |
|   |   |   |   |

