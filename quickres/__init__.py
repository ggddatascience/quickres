import GPUtil
import rasterio
from rasterio import windows, Affine
import numpy as np
import keras
import tqdm
import tensorflow as tf
import glob
import os
from argparse import ArgumentParser

import logging

log = logging.getLogger()


def quick_res(in_file, out_file, model):
    #  model_file='/osn2/model/hdnn/superres_v3.0_2018-01-25 22:38:29.338372.h5'):
    img = rasterio.open(in_file)
    profile = img.profile
    profile['height'] *= 2
    profile['width'] *= 2
    if 'vrt' in profile['driver'].lower():
        profile['driver'] = 'GTiff'
    profile['transform'] *= Affine.scale(.5)
    s = 512//2
    apron = 2
    print(profile)
    h, w = img.shape[0], img.shape[1]

    with rasterio.open(out_file, 'w', **profile) as out:
        print(out_file, img.shape[0] // s, img.shape[1] // s)
        pbar = tqdm.tqdm(total=img.shape[0] * img.shape[1] // (s * s))
        maxint = 65535 if profile['dtype'] == 'uint16' else 255
        for l in range(0, img.shape[0], s):
            for m in range(0, img.shape[1], s):
                pbar.update(1)
                chunk = img.read(window=((l - apron, (l + s) + apron), (m - apron, m + s + apron)), boundless=True)
                if np.max(chunk) == 0:
                    continue

                end_l = h * 2 if l + s > h else 2 * (l + s)
                end_m = w * 2 if m + s > w else 2 * (m + s)
                win = windows.Window.from_ranges((2 * l, end_l), (2 * m, end_m))
                pred = model.predict(np.expand_dims(chunk, -1))
                if isinstance(pred, list):
                    pred = pred[0]

                hd = pred.reshape(img.count, (s + 2 * apron) * 2, (s + 2 * apron) * 2)
                hd = np.clip(hd, 1, maxint)[:, 2 * apron:-2 * apron, 2 * apron:-2 * apron]
                hd = hd.astype(img.dtypes[0])

                out.write(hd, list(range(1, img.count+1)), window=win)
    img.close()
    pbar.close()


def main():
    parser = ArgumentParser()
    parser.add_argument('input', help="Directory of images to apply quickres. "
                                      "Also can be single image [e.g. /osn/phase2/pan/ , /osn/xv/ms/1040058234.tif")
    parser.add_argument('output', help="Directory (or Output Location + Filename if singe Input Image) "
                                       "to deliver HD image(s). ")
    parser.add_argument("--overwrite", action="store_true", help="overwrite files in output directory")
    parser.add_argument("--model", default='/osn2/model/hdnn/superres_v3.0_2018-01-25 22:38:29.338372.h5',
                        help="Location of QuickRes model to be used")
    args = parser.parse_args()

    # Get the first available GPU
    # Set CUDA_VISIBLE_DEVICES to mask out all other GPUs than the first available device id
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(GPUtil.getFirstAvailable()[0])

    model = keras.models.load_model(args.model, custom_objects={"tf": tf})
    if args.input.lower().endswith('tif'):
        tif_name = os.path.split(args.input)[1]
        output = args.output if args.output.lower().endswith(".tif") else os.path.join(args.output, tif_name)
        quick_res(args.input, output, model)
    else:
        for tif in glob.glob(args.input + "/*.tif") + glob.glob(args.input + "/*.TIF"):
            tif_name = os.path.split(tif)[1]
            logging.info("QuickRes applied on {}".format(tif_name), )
            out_tif = os.path.join(args.output, tif_name)
            if not args.overwrite and os.path.exists(out_tif):
                continue
            quick_res(tif, out_tif, model)
    from keras import backend as K
    K.clear_session()


if __name__ == '__main__':
    main()
