from setuptools import setup, find_packages

setup(
    name='quickres',
    version='2018.08.03.1',
    description='A package to apply quickres to imagery',
    url='https://radiantsolutions.com',
    author='Mahmoud Lababidi',
    author_email='mla@mla.im',
    license='Apache',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=['tqdm', 'rasterio', 'numpy', 'keras', 'gputil'],
    python_requires='>=3',
    entry_points={
        'console_scripts': [
            'quickres=quickres:main',
        ],
    },
)
